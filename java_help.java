import java.util.*;
import java.io.*;


public class java_help {

    // Hashmap to store words and their hamming distance from target
    public static HashMap<String, Integer> word_dict = new HashMap<String, Integer>();
    // Hashset to keep track of visited nodes
    public static Set<String> visited = new HashSet<>();
    // Queue
    public static ArrayList<node> queue = new ArrayList<node>();

    // Calculates hamming distance
    public static int hamming_distance(String w1, String w2)
    {
        int distance = 0;

        for (int i = 0; i < w1.length(); i ++)
        {
            if (w1.charAt(i) != w2.charAt(i))
                distance ++;
        }

        return distance;
    }

    // Class to store nodes in the queue
    public static class node
    {
        public Integer distance;
        public String word;
        public String path;

        public node(int distance, String value, String path)
        {
            this.distance = distance;
            this.word = value;
            this.path = path;
        }
    }

    public static void main(String args[]) throws Exception
    {
        String in = args[0];
        String out = args[1];

        // Read the words file into a hashmap where key is word, value is the hamming distance from the target
        BufferedReader br = new BufferedReader(new FileReader(new File("words_2.txt")));
        String s= br.readLine();

        while(s != null)
        {
            if(s.length() == in.length())
                word_dict.put(s.toUpperCase(), hamming_distance(s.toUpperCase(), out));

            s = br.readLine();
        }

        // Add the starting word to the queue and visited list
        visited.add(in);
        queue.add(new node(word_dict.get(in), in, in));

        System.out.println(func(out));
    }

    public static String func(String target)
    {
        node current = queue.remove(0);
        String word = current.word;
        String path = current.path;

        
        // Base case: is the current word the target?
        if(word.equals(target))
            return current.path;

        for (int n = 0; n < word.length(); n++)
        {
            for (int i = 39; i < 90; i++)
            {
                // Construct the new word
                String newWord = word.substring(0, n) + (char)i + word.substring(n+1);
            
                if(!visited.contains(newWord) && word_dict.containsKey(newWord))
                {
                    // Add the new word to the queue and mark it visited
                    queue.add(new node(word_dict.get(newWord), newWord, (path + " -> " + newWord)));
                    visited.add(newWord);
                }
            }
        }

        // Sort the queue
        queue.sort((node n1, node n2)->n1.distance-n2.distance);

        if(queue.size() == 0)
            return "No word ladder exists";

         // Recursive step
         return func(target);
    }
}

