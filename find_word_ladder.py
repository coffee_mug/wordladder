import Queue
import sys
import networkx as nx
from matplotlib.pyplot import pause
import pylab

pylab.ion()

class Node:
    def __init__(self, path, state):
        self.path = path    # Word ladder
        self.state = state  # Word in the dictionary
    
    
def extend(word):
    new_words = set()
    
    for n in range(0,4):
        for new_char in letters:
            new_words.add(word[0:n] + new_char + word[n+1:])
            
    return new_words


g = nx.Graph()

queue = Queue.Queue()
start_word = str(sys.argv[1])
end_word = str(sys.argv[2])
letters = ["'", 'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','W','X','Y','Z']
words = set()
visited = set()

words_file = open("words_2.txt","r")

for line in words_file:
    line = line.replace("\n","").upper()
    if len(line) == len(start_word):
        words.add(line)


if start_word not in words:
    print('The start word is not in the dictionary')

if end_word not in words:
    print('The end word is not in the dictionary')

# Put the first item in the queue
queue.put(Node(start_word, start_word), False)
g.add_node(start_word, label=start_word)

while(not queue.empty()):
    current_node = queue.get()
    
    # The current node is the target one
    if current_node.state == end_word:
        print("Word ladder found!")
        print(current_node.path)
        nx.draw(g)
        pylab.show()
        quit()
        
    # Compute one step extensions
    candidate_words = extend(current_node.state)
    new_words = candidate_words.intersection(words)
    new_words = new_words - visited
    
    # Add new words to list
    for word in new_words:
        queue.put(Node(current_node.path + ' ' + word, word), False)
        visited.add(word)
        g.add_node(word, label=word, color='blue')
        g.add_edge(current_node.state, word)

nx.draw(g, with_labels=True, node_size=80, font_size=8)
pylab.show()

print("No word ladder found.")
